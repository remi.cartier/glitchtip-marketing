import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ScullyLibModule } from '@scullyio/ng-lib';
import { PricingRoutingModule } from './pricing-routing.module';
import { PricingComponent } from './pricing.component';
import { MatCardModule } from '@angular/material/card';
import { SharedModule } from '../shared/shared.module';
@NgModule({
  declarations: [PricingComponent],
  imports: [
    CommonModule,
    PricingRoutingModule,
    ScullyLibModule,
    MatCardModule,
    SharedModule,
  ],
})
export class PricingModule {}
